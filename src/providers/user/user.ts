import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: HttpClient) {
    console.log('Hello UserProvider Provider');
  }

  passwordreset(email) {
    var promise = new Promise((resolve, reject) => {
      firebase.auth().sendPasswordResetEmail(email).then(() => {
        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }

  addContact(contactObjs: any[]) {
    localStorage.setItem('contacts', JSON.stringify(contactObjs));
  }

  getContacts() {
    const contacts = localStorage.getItem('contacts');
    if (contacts) {
      return JSON.parse(contacts);
    }
    return [];
  }
}
