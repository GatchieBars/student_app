export interface SoundboardMap {
    button: ButtonMap;
    soundPath: string;
}

interface ButtonMap {
    name: string;
    color: string;
}

// Mocks

export const SoundboardMock = [
    {
    button: {
        name: 'Vagues',
        color: '#3498db'
    },
    soundPath: 'sounds/Buzz-SoundBible.com-1790490578.mp3'
},
{
    button: {
        name: 'Applaud',
        color: '#2ecc71'
    },
    soundPath: 'sounds/Buzz-SoundBible.com-1790490578.mp3'
}
]