import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from "angularfire2/auth";
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { AuthProvider } from '../providers/auth/auth';
import { AngularFireAuth } from 'angularfire2/auth';


import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { AlertPage } from '../pages/alert/alert';
import { ProfilePage } from '../pages/profile/profile';
import { PasswordresetPage } from '../pages/passwordreset/passwordreset';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Flashlight } from '@ionic-native/flashlight';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { SMS } from "@ionic-native/sms";
import { Media } from '@ionic-native/media';
import { UserProvider } from '../providers/user/user';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';

export const FIREBASE_CONFIG = {
  apiKey: "AIzaSyDKjmdlkyTNCcOUUkH933j4BLsu-0Irex0",
  authDomain: "specialert-a0d40.firebaseapp.com",
  databaseURL: "https://specialert-a0d40.firebaseio.com",
  projectId: "specialert-a0d40",
  storageBucket: "specialert-a0d40.appspot.com",
  messagingSenderId: "1054049090950"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    AlertPage,
    ProfilePage,
    PasswordresetPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    AlertPage,
    ProfilePage,
    PasswordresetPage
  ],
  providers: [
    StatusBar,
    BackgroundGeolocation,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    AngularFireAuth,
    TextToSpeech,
    UserProvider,
    SMS,
    Flashlight,
    Media
  ]
})
export class AppModule {}
