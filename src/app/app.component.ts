import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(
    platform: Platform, statusBar: StatusBar, 
    splashScreen: SplashScreen,
    private backgroundGeo: BackgroundGeolocation
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      // this.backgroundGeoServ();
    });
  }

  backgroundGeoServ(): void {
    // const config: BackgroundGeolocationConfig = { 
    //   desiredAccuracy: 10,
    //   stationaryRadius: 20,
    //   distanceFilter: 30,
    //   debug: true,
    //   stopOnTerminate: false
    // }
    // this.backgroundGeo.configure(config).subscribe((loc: BackgroundGeolocationResponse) => {
    //   console.log("GEO===");
    //   console.log("GEO===", loc.longitude);
     
    // });

    
    // this.backgroundGeo.start();
    // this.backgroundGeo.getLocations()
    // .then((stationaryLoc: any) => {
    //   console.log(stationaryLoc, "------")
    // }).then((err) => console.log('ERRR', err))
  }
}
