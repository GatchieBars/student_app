import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

export interface Contacts {
  contactName: string;
  contactNumber: string;
}
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  contacts: Array<Contacts> = [];
  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private userServ: UserProvider
  
  ) {

  }

  ionViewWillEnter() {
    this.contacts = this.userServ.getContacts();
  }
  addContact() {
    const prompt = this.alertCtrl.create({
      title: 'Input Contact',
      inputs: [
        { name:'contactName', placeholder:'Enter contact name' },
        { name: 'contactNumber', placeholder: 'Enter contact number', type: 'number'}
      ],
      buttons: [
        { text:'Cancel' },
        { text: 'Save', handler:(data: Contacts) => {
          if (data.contactName && data.contactNumber) {
            this.contacts.push(data);
            this.userServ.addContact(this.contacts);
          }
        }}
      ]
    })
    prompt.present();
  }
  editContact(ind) {
    let contact: Contacts = this.contacts[ind];
    const prompt = this.alertCtrl.create({
      title: 'Input Contact',
      inputs: [
        { name:'contactName', placeholder:'Enter contact name', value: contact.contactName },
        { name:'contactNumber', placeholder:'Enter contact name', value: contact.contactNumber },
      ],
      buttons: [
        { text:'Cancel' },
        { text: 'Save', handler:(data: Contacts) => {
          if (data.contactName && data.contactNumber) {
              this.contacts[ind].contactName = data.contactName;
              this.contacts[ind].contactNumber = data.contactNumber;
              this.userServ.addContact(this.contacts);
          }
        }}
      ]
    })
    prompt.present();
  }
}
