import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { AlertPage } from '../alert/alert';
import { ProfilePage } from '../profile/profile';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab4Root = AlertPage;
  tab5Root = ProfilePage;
  tab7Root = AboutPage;
  tab8Root = ContactPage;


  constructor() {

  }
}
