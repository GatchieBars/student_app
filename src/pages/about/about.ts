import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TextToSpeech } from '@ionic-native/text-to-speech';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  text: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private tts: TextToSpeech) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }
  async sayText():Promise<any>{
    try{
      await this.tts.speak(this.text);
      console.log("Successfully spoke" + this.text)
    }
    catch(e){
      console.log(e);
    }
  }
  async sayText1():Promise<any>{
    try{
      await this.tts.speak('can you help me?');
      console.log("Successfully spoke" + this.text)
    }
    catch(e){
      console.log(e);
    }
  }
   async sayText2():Promise<any>{
    try{
      await this.tts.speak('excuse me. can i ask you a question?');
      console.log("Successfully spoke" + this.text)
    }
    catch(e){
      console.log(e);
    }
  }
  async sayText3():Promise<any>{
    try{
      await this.tts.speak('can you please help me cross the street?');
      console.log("Successfully spoke" + this.text)
    }
    catch(e){
      console.log(e);
    }
  }
  async sayText4():Promise<any>{
    try{
      await this.tts.speak('excuse me? which way is the restroom?');
      console.log("Successfully spoke" + this.text)
    }
    catch(e){
      console.log(e);
    }
  }
}
