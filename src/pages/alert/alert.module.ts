import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlertPage } from './alert';
import { Flashlight } from '@ionic-native/flashlight';




@NgModule({
  declarations: [
    AlertPage,
  ],
  imports: [
    IonicPageModule.forChild(AlertPage),
    Flashlight,
  ],
})
export class AlertPageModule {
}
