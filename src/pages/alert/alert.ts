  import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Flashlight } from '@ionic-native/flashlight';
import { Media, MediaObject } from '@ionic-native/media';


import * as SoundboardMapping from '../../models/soundboard.mapping';


@IonicPage()
@Component({
  selector: 'page-alert',
  templateUrl: 'alert.html',
})
export class AlertPage {
  isOn: boolean = false;
  private soundboardData: SoundboardMapping.SoundboardMap[];
  private file: MediaObject;

  constructor(
    public navCtrl: NavController,
    private flashLight: Flashlight,
    public navParams: NavParams,
    private media: Media) {
      this.soundboardData = SoundboardMapping.SoundboardMock;
  }

  public playsound(ressource: string) {
    if (this.file) {
      this.file.stop();
      this.file.release();
    }
    this.file = this.media.create('./assets/' + ressource);
    this.file.play()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlertPage');
  }

  async isAvailable(): Promise<boolean> {
    try {
      return await this.flashLight.available();
    }
    catch (e) {
      console.log(e);
    }
  }

  async toggleFlash(): Promise<void> {
    try {
      let available = await this.isAvailable();
      if (available) {
        await this.flashLight.toggle();
        this.isOn = !this.isOn;
      }
      else {
        console.log("Isn't available.");
      }
    }
    catch (e) {
      console.log(e);
    }
  }

  async turnOnFlash():Promise<void>{
    await this.flashLight.switchOn();
    }
  async turnOffFlash():Promise<void>{
    await this.flashLight.switchOff();
    }
  async isFlashOn():Promise<boolean>{
    return await this.flashLight.isSwitchedOn();
    }

}
