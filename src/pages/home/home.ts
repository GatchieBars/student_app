import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { SMS } from "@ionic-native/sms";
import { CallNumber } from "@ionic-native/call-number";
import { UserProvider } from '../../providers/user/user';
import { Contacts } from '../contact/contact';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    private sms: SMS,
    private alertCtrl: AlertController,
    private userServ: UserProvider
  ) {

  }

  sendSMS1() {
    var options: {
      replaceLineBreaks: true,
      android: {
        intent: 'INTENT'
      }
    }
    this.sms.send('09055394517', 'Send help. Im in danger.', options).then(() => {
      console.log('sms worked');
    }).catch((err) => {
      alert(JSON.stringify(err))
    });
  }
  sendSMS2() {
    var options: {
      replaceLineBreaks: true,
      android: {
        intent: 'INTENT'
      }
    }
    this.sms.send('09055394517', 'Assistance needed. Please check my current location and send help.', options).then(() => {
      console.log('sms worked');
    }).catch((err) => {
      alert(JSON.stringify(err))
    });
  }
  sendSMS3() {
    var options: {
      replaceLineBreaks: true,
      android: {
        intent: 'INTENT'
      }
    }
    this.sms.send('09055394517', 'Im in danger. Please call some help ASAP.', options).then(() => {
      console.log('sms worked');
    }).catch((err) => {
      alert(JSON.stringify(err))
    });
  }

  sendSms(opts){
    const messages = [
      'Send help. Im in danger.',
      'Assistance needed. Please check my current location and send help.',
      'Im in danger. Please call some help ASAP.'
    ];
    const emergencyMessage = messages[opts];
    const contacts: Contacts[] = this.userServ.getContacts();

    if (Array.isArray (contacts) && contacts.length) {
      contacts.forEach( async (contact) => {
        await this.deliverSMS(contact.contactNumber, emergencyMessage);
      })
      this.showMessageSent();
    }
    
  }

  showMessageSent(){
    const alrt = this.alertCtrl.create({
      title:'Success',
      message: 'Alert successfully sent'
    });

    alrt.present();
  }

  deliverSMS(receipientNum: any, message){
    var options: {
      replaceLineBreaks: true,
      android: {
        intent: 'INTENT'
      }
    }
    if ('undefined' !== receipientNum) {
      return new Promise((resolve, reject) => {
        this.sms.send(receipientNum, message , options)
          .then(() => resolve())
          .catch(() => reject())
      })
    }
    return null;
  }

}
